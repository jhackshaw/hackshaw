from django.conf.urls import url, include
from django.contrib import admin
from django.views.defaults import page_not_found
from user.views import LoginView, LogoutView, RegisterView, AuthorRequestView
from core.views import RedirectView
from feed.views import OverviewFeed

urlpatterns = [
    # authentication urls
    url(r'^login$', LoginView.as_view(), name='login'),
    url(r'^register$', RegisterView.as_view(), name='register'),
    url(r'^author_request$', AuthorRequestView.as_view(), name='author-request'),
    url(r'^logout$', LogoutView.as_view(), name='logout'),

    # home page is the overview feed
    url(r'^$', OverviewFeed.as_view(), name='homepage'),

    # user profile urls
    url(r'^u/', include('user.urls', namespace='user')),

    # feed urls
    url(r'^f/', include('feed.urls', namespace='feed')),

    # article/ question/ survey/ etc.
    url(r'^', include('blog.urls', namespace='blog')),

    # admin urls
    url(r'^admin/login', RedirectView.as_view(), {'url': 'login'}),
    url(r'^admin/', admin.site.urls)

]
