from django.forms import ModelForm, ModelChoiceField, Textarea, CharField, Select, ChoiceField
from django.urls import reverse
from .models import Article
from core.models import Tag
from feed.models import Feed
from django.contrib.auth import get_user, get_user_model
from django.contrib.contenttypes.models import ContentType
import re

TAG_RE = re.compile(r'[a-zA-Z0-9-!+]{,20}')
def valid_tag(tag):
    return bool(re.match(TAG_RE, tag))


class ItemAdminBaseForm(ModelForm):
    feed = ModelChoiceField(queryset=Feed.objects.all())
    tag = CharField(max_length=255, required=False)

    class Meta:
        fields = ['feed', 'published', 'headline', 'tag']
        widgets = {}

    def clean_tag(self):
        data = self.cleaned_data.get('tag', '')
        ret = [Tag.objects.get_or_create(name=t)[0] for t in data.split(' ')[:10]
                                                    if valid_tag(t)]
        return ret

    def save(self, *args, **kwargs):
        f = self.cleaned_data['feed']
        self.instance.content_type = ContentType.objects.get_for_model(f)
        self.instance.object_id = f.id
        instance = super(ItemAdminBaseForm, self).save(*args, **kwargs)
        instance.tags = self.cleaned_data['tag']
        return instance
    

class ItemBaseForm(ItemAdminBaseForm):
    # add fields required for the associated FeedEntry
    feed = ModelChoiceField(queryset=Feed.objects.none())

    def __init__(self, *args, **kwargs):
        # # pull out extra kwargs
        current_user = kwargs.pop('current_user', None)
        if current_user is None:
            raise ValueError('Must specify either current_user or feed_queryset when initializing a BlogItemForm')

        #  init super with remaining args
        super(ItemBaseForm, self).__init__(*args, **kwargs)
        self.fields['feed'].queryset = Feed.objects.filter(user=current_user)


class ArticleForm(ItemBaseForm):
    class Meta(ItemBaseForm.Meta):
        model = Article
        fields = ItemBaseForm.Meta.fields + ['markdown']
        widgets = {**ItemBaseForm.Meta.widgets, **{'markdown': Textarea()}}


class ArticleAdminForm(ItemAdminBaseForm):
    class Meta(ArticleForm.Meta):
        pass