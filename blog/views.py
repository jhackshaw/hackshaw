from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic import DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from .forms import ArticleForm
from .models import Article, FeedItem

User = get_user_model()


class DetailItemBase(DetailView):
    def get_queryset(self):
        return FeedItem.objects.filter(feeds__user__username=self.kwargs['username'],
                                       feeds__slug=self.kwargs['feedslug'])


class CreateItemBase(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    success_url = '/'
    template_name = 'items/feed_item_form.html'
    permission_required = 'blog.create_feed_item'
    login_url = reverse_lazy('author-request')

    def get_form_kwargs(self):
        kwargs = super(CreateItemBase, self).get_form_kwargs()
        kwargs['current_user'] = self.request.user
        return kwargs

    def handle_no_permission(self):
        print('handling no permission')
        return super(CreateItemBase, self).handle_no_permission()


class EditItemBase(LoginRequiredMixin, UpdateView):
    template_name = 'items/feed_item_form.html'
    
    def get_form_kwargs(self):
        kwargs = super(EditItemBase, self).get_form_kwargs()
        kwargs['current_user'] = self.request.user
        return kwargs

    def get_initial(self):
        initial = super(EditItemBase, self).get_initial()
        initial['tag'] = " ".join([t.name for t in self.object.tags.all()])
        initial['feed'] = self.object.feed.id
        return initial

    def get_queryset(self):
        if self.request.user.username != self.kwargs['username']:
            raise PermissionDenied
        return FeedItem.objects.filter(feeds__user__username=self.kwargs['username'],
                                       feeds__slug=self.kwargs['feedslug'])


class ArticleCreateView(CreateItemBase):
    form_class = ArticleForm


class ArticleEditView(EditItemBase):
    form_class = ArticleForm
    model = Article

    def get_object(self, *args, **kwargs):
        feeditem = super(ArticleEditView, self).get_object(*args, **kwargs)
        return getattr(feeditem, 'article', None)

    def get_initial(self):
        initial = super(ArticleEditView, self).get_initial()
        initial['markdown'] = self.object.article.markdown
        return initial


class ArticleDetailView(DetailItemBase):
    template_name = 'article/article_detail.html'


class QuestionDetailView(DetailItemBase):
    model = Article
    template_name = 'question/question_detail.html'


class QuestionCreateView(LoginRequiredMixin, CreateView):
    model = Article
    template_name = 'question/question_form.html'



                                