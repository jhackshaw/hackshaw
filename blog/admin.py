from django.contrib import admin
from .models import Article
from .forms import ArticleAdminForm



def field_from_feed(field):
    def field_of_feed(obj):
        return getattr(obj.feed, field)
    return field_of_feed

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    list_display = ('headline', 
                    field_from_feed('user'),
                    field_from_feed('name'), 
                    'created')