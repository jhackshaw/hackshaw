from django.conf.urls import url, include
from . import views


article_patterns = [
    url('create', views.ArticleCreateView.as_view(), name='create'),
    url(r'(?P<username>[a-zA-Z0-9_-]{1,20})/(?P<feedslug>[a-zA-Z0-9_-]{1,100})/(?P<slug>[a-zA-Z0-9_-]{1,100})/edit$',
        views.ArticleEditView.as_view(),
        name='edit'),
    url(r'(?P<username>[a-zA-Z0-9_-]{1,20})/(?P<feedslug>[a-zA-Z0-9_-]{1,100})/(?P<slug>[a-zA-Z0-9_-]{1,100})$', 
        views.ArticleDetailView.as_view(), 
        name='permalink'),
]

# question_patterns = [
#     url('create', views.CreateQuestionView.as_view(), name='create')
# ]


# url_patterns = [
#     url('create', views.CreateUrlView.as_view(), name='create')
# ]

urlpatterns = [
    # permalink for post
    # p/username/feedslug/postslug
    # url(r'p/(?P<username>[a-zA-Z0-9_-]{1,20})/(?P<feedslug>[a-zA-Z0-9_-]{1,50})/(?P<slug>[a-zA-Z0-9_-]{1,80})$', 
    #     views.ViewFeedItem.as_view(),
    #     name='permalink'),
        
    url('^article/', include(article_patterns, namespace='article')),
    # url('^question/', include(question_patterns, namespace='question')),
    # url('^url/', include(url_patterns, namespace='url'))
]