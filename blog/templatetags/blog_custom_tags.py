from django import template

register = template.Library()


@register.simple_tag
def user_gravatar_url(user, size):
    return user.get_gravatar_url(size=size)


@register.simple_tag
def article_lightbulb_count(post):
    return post.tags.count()

@register.simple_tag
def feed_post_count(feed):
    return feed.posts.count()

@register.simple_tag
def user_feed_list(user):
    return user.feeds.all()