import random
import faker
from django.core.management import BaseCommand
from feed.models import Feed
from core.models import Tag
from user.models import User


fake = faker.Faker()


class Command(BaseCommand):
    help = "Create fake feeds as test data"

    def add_arguments(self, parser):
        parser.add_argument('--min-per-user',
                            dest='min_per_user',
                            type=int,
                            default=0,
                            help='minimum number of feeds per user')
        parser.add_argument('--max-per-user',
                            dest='max_per_user',
                            type=int,
                            default=10,
                            help='maximum number of feeds per user')
        parser.add_argument('--min-tags',
                            dest='min_tags',
                            type=int,
                            default=0,
                            help='minimum number of tags per feed')
        parser.add_argument('--max-tags',
                            dest='max_tags',
                            type=int,
                            default=10,
                            help='maximum number of tags per feed')

    def handle(self, *args, **options):
        users = User.objects.all()
        tags = list(Tag.objects.all())
        if len(users) < 1:
            print('there are no users to create feeds for!')
        if len(tags) < options['max_tags']:
            print('please specify a number of maximum tags less than the number that exist')
            return
        for u in users:
            for _ in range(random.randint(options['min_per_user'], 
                                          options['max_per_user'])):
                to_name = fake.catch_phrase()
                while Feed.objects.filter(user=u, name=to_name).first() is not None:
                    to_name = fake.catch_phrase()
                    print('skipping feed that already exists')
                f = Feed.objects.create(user=u, name=to_name)
                f.tags = random.sample(tags, 
                                       random.randint(options['min_tags'], 
                                                      options['max_tags']))
                print(f.__repr__())