import random
import faker
from django.core.management import BaseCommand
from django.template.defaultfilters import slugify
from blog.models import Article
from blog.forms import ArticleForm
from core.models import Tag
from user.models import User
from feed.models import Feed, FeedItem


fake = faker.Faker()
fake_markdown_prefix = """

# my header

> its not the size of the dog in the fight or something

```python
import re
TAG_RE = re.compile(r'whatsinaname')

def is_tag(tag):
    return re.march(TAG_RE, tag)

print(is_tag('tagme'))
```


First Header  | Second Header
----------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell


*and* **finally** bold and italics

"""


class Command(BaseCommand):
    help = "Create fake posts as test data"

    def add_arguments(self, parser):
        parser.add_argument('--min-per-feed',
                            dest='min_per_feed',
                            type=int,
                            default=0,
                            help='minimum number of posts per feed')
        parser.add_argument('--max-per-feed',
                            dest='max_per_feed',
                            type=int,
                            default=10,
                            help='maximum number of posts per feed')
        parser.add_argument('--min-tags',
                            dest='min_tags',
                            type=int,
                            default=0,
                            help='minimum number of tags per post')
        parser.add_argument('--max-tags',
                            dest='max_tags',
                            type=int,
                            default=10,
                            help='maximum number of tags per post')

    def handle(self, *args, **options):
        feeds = Feed.objects.all()
        tags = list(Tag.objects.all())
        if len(feeds) < 1:
            print('there are no feeds to create posts for!')
        if len(tags) < options['max_tags']:
            print('please specify a number of maximum tags less than the number that exist')
            return
        for f in feeds:
            for _ in range(random.randint(options['min_per_feed'], 
                                          options['max_per_feed'])):
                to_name = fake.catch_phrase()
                while FeedItem.objects.filter(feeds__id=f.id, slug=slugify(to_name)).first() is not None:
                    to_name = fake.catch_phrase()
                    print('skipped post that already exists')
                form = ArticleForm({'feed': f.id,
                                    'headline': to_name,
                                    'markdown': fake_markdown_prefix + "\n\n".join(fake.paragraphs()),
                                    'published': True},
                                    current_user=f.user)
                form.is_valid()
                article = form.save()
                article.tags = random.sample(tags, 
                                             random.randint(options['min_tags'], 
                                                            options['max_tags']))
                print(article.__repr__())