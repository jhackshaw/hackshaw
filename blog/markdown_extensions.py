from markdown.treeprocessors import Treeprocessor
from markdown.inlinepatterns import Pattern
from markdown.extensions import Extension
from markdown.util import etree
import copy

#<iframe width="854" height="480" src="https://www.youtube.com/embed/9bZkp7q19f0"
# frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>

YOUTUBE_PATTERN = r'\[youtube\]\(.+v=(.*)\)'
YOUTUBE_EMBED_URL = "https://www.youtube.com/embed/"


class YoutubeEmbedExtension(Pattern):
    """
        Extends markdown to allow [youtube](youtube url) which will be converted
        into a responsive embed. Potential security issue, although the youtube.com domain
        is hardcoded so I don't really think so.
        Only users with the blog.embed_content are allowed to use this
    """
    def handleMatch(self, m):
        print('group2:', m.group(2))
        wrapper = etree.Element('div')
        wrapper.set('class', 'video-container')
        el = etree.SubElement(wrapper, 'iframe')
        el.set('src', YOUTUBE_EMBED_URL + m.group(2))
        el.set('allow', 'encrypted-media')
        el.set('frameborder', '0')
        el.set('gesture', 'media')
        el.set('width', '854')
        el.set('height', '480')
        el.set('allowfullscreen', 'true')
        return wrapper


class SetParagraphClass(Treeprocessor):
    """
        Adds the class flow-text to all p elements for responsive fonts from
        materializecss
    """
    def run(self, root):
        self.add_class_to_p(root)

    def add_class_to_p(self, element):
        for child in element:
            if child.tag == 'p':
                child.set('class', 'flow-text')
            else:
                self.add_class_to_p(child)


class SPEmbedExtension(Extension):
    def extendMarkdown(self, md, md_globals):
        md.inlinePatterns.add('youtube_embed', YoutubeEmbedExtension(YOUTUBE_PATTERN), '_begin')


class SPClassesExtension(Extension):
    def extendMarkdown(self, md, md_globals):
        md.treeprocessors.add('pclasses', SetParagraphClass(md), '_end')