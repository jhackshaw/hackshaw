from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model
from gm2m import GM2MField
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from .markdown_extensions import SPEmbedExtension, SPClassesExtension
import markdown
import bleach


# Markdown parameters for users without the blog.embed_content permission
extensions = ['markdown.extensions.fenced_code',
              'markdown.extensions.codehilite',
              'markdown.extensions.nl2br',
              'markdown.extensions.tables',
              SPClassesExtension()]
allowed_tags = set(['h1', 'h2', 'h3', 'h4', 'b', 'em', 
                    'ul', 'ol', 'li', 'code', 'blockquote',
                    'p', 'strong', 'pre', 'table', 'tr', 'td', 
                    'th', 'tbody', 'thead', 'div', 'span', 'br', 'a'])
allowed_attributes = ['class']

# extra markdown parameters for users WITH the blog.embed_content permission
# potentially unsafe, but I think probably not
embed_allowed_tags = allowed_tags.copy()
embed_allowed_tags.update(['script', 'iframe'])
embed_allowed_attributes = allowed_attributes + ['href', 'src', 'allow', 'allowfullscreen', 
                                                 'frameborder', 'height', 'gesture']
embed_extensions = extensions + [SPEmbedExtension()]


def markdown_to_html(md, with_embeds=False):
    if with_embeds:
        html = bleach.clean(markdown.markdown(md, extensions=embed_extensions),
                                              tags=embed_allowed_tags,
                                              attributes=embed_allowed_attributes)
    else:
        html = bleach.clean(markdown.markdown(md, extensions=extensions),
                                              tags=allowed_tags,
                                              attributes=allowed_attributes)
    return bleach.linkify(html)


class FeedItem(models.Model):
    """ Abstract model containing fields common to all types of feed entries """
    published_choices = ((False, 'Draft'), (True, 'Publish'))
    
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    # fields defining this entry: these are common to all types of entries
    # content_object contains specific entry with specific fields
    slug = models.SlugField(max_length=80)
    headline = models.CharField(max_length=80)
    published = models.BooleanField(default=False, choices=published_choices)
    tags = GM2MField('core.Tag')
    
    # required fields for the generic relationship
    # content_object is the instance of FeedItemBase
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    feed = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ('-created',)
        default_related_name = '%(model_name)s'
        unique_together = ['object_id', 'slug']
        permissions = (
            ('create_feed_item', 'create a feed item or one of its subclasses'),
            ('embed_content', 'embed content into a feeditem - security purposes')
        )

    @property
    def short_entry_template(self):
        return self.child_object.short_entry_template

    @property
    def child_object(self):
        for f in self._meta.related_objects:
            if isinstance(f, models.OneToOneRel) and hasattr(self, f.related_model.__name__.lower()):
                return getattr(self, f.related_model.__name__.lower())

    def save(self, *args, **kwargs):
        self.slug = slugify(self.headline)
        return super(FeedItem, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return self.child_object.get_absolute_url()

    def __repr__(self):
        return '<FeedItemBase type=%r headline=%r>' % (self.__class__.__name__, self.headline)


class Article(FeedItem):
    markdown = models.TextField()
    html = models.TextField()
    summary = models.CharField(max_length=300)
    short_entry_template = 'article/article.html'

    def save(self, *args, **kwargs):
        self.html = markdown_to_html(self.markdown,
                                     with_embeds=self.feed.user.has_perm('blog.embed_content'))
        self.summary = bleach.clean(self.html, tags=[], strip=True)[:299]
        return super(Article, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('blog:article:permalink', kwargs={'username': self.feed.user.username,
                                                         'feedslug': self.feed.slug,
                                                         'slug': self.slug})
    
    @property
    def permalink(self):
        return self.get_absolute_url()

    def __repr__(self):
        return '<Article %r>' % self.headline
