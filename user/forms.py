from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm, Textarea
from .models import User


class RegisterForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + ('email',)


class ProfileEditForm(ModelForm):
    class Meta:
        model = User
        fields = ['email', 'github_link', 'linkedin_link', 'about_me']
        widgets = {'about_me': Textarea()}


class AdminForm(ModelForm):
    class Meta:
        model = User
        fields = UserCreationForm.Meta.fields + \
                 ('email', 'about_me', 'is_active', 'is_admin', 'groups')
