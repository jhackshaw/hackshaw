from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, RegistrationRequest, Notification
from .forms import AdminForm


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = AdminForm
    add_form = AdminForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password', 'username', 'about_me', 'github_link', 'linkedin_link')}),
        ('Permissions', {'fields': ('is_admin', 'groups')}),
    )
    add_fieldsets = fieldsets
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


@admin.register(RegistrationRequest)
class RegistrationRequestAdmin(admin.ModelAdmin):
    list_display = ('user', 'about')


@admin.register(Notification)
class RegistrationRequestAdmin(admin.ModelAdmin):
    list_display = ('user', 'text')