from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse
from django.views.generic import DetailView, View, ListView
from django.views.generic.edit import UpdateView, CreateView, FormView
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from .forms import RegisterForm, ProfileEditForm
from .models import RegistrationRequest, Notification   
from feed.models import Feed
from core.models import Tag


class RegisterView(FormView):
    template_name = "registration/register.html"
    form_class = RegisterForm
    success_url = reverse_lazy('feed:overview')

    def form_valid(self, form):
        new_user = super(RegisterView, self).form_valid(form)
        new_user.notify('Welcome to SemperPy! Have a look around. Feedback is always appreciated.')
        return new_user


class AuthorRequestView(LoginRequiredMixin, CreateView):
    model = RegistrationRequest
    fields = ['about']
    template_name = 'registration/author_request.html'
    success_url = reverse_lazy('feed:overview')

    def form_valid(self, form):
        req = form.instance.user = self.request.user
        self.request.user.notify('Your authorship request has been recieved, check back later!')
        return super(AuthorRequestView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        if RegistrationRequest.objects.filter(user=request.user).exists():
            return HttpResponseRedirect('/')
        return super(AuthorRequestView, self).get(request, *args, **kwargs)


class LoginView(LoginView):
    pass


class LogoutView(LogoutView):
    pass


class ProfileView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    slug_field = 'username'
    slug_url_kwarg = 'username'
    context_object_name = 'profile_of'


class ProfileEditView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = ProfileEditForm
    slug_field = 'username'
    slug_url_kwarg = 'username'
    template_name_suffix = '_edit'


class NotificationsView(LoginRequiredMixin, ListView):
    model = Notification
    paginate_by = 20
    paginate_orphans = 5
    template_name = 'user/user_notifications.html'

    def get_queryset(self):
        return self.request.user.notifications.all()


class DeleteNotificationView(LoginRequiredMixin, View):
    def get(self, request, pk):
        delete = Notification.objects.filter(user=self.request.user,
                                             pk=pk).delete()
        status = 'deleted' if delete[0] > 0 else 'not_deleted'
        return JsonResponse({'status': status})