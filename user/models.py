from django.db import models
from django.urls import reverse
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin, Group
from django.contrib.auth.validators import ASCIIUsernameValidator
import hashlib
from blog.models import FeedItem



class CustomUserManager(BaseUserManager):
    def create_user(self, username, email, password):
        user = self.init_user(username, email, password)
        user.save()
        return user
    
    def create_superuser(self, username, email, password):
        user = self.init_user(username, email, password)
        user.is_admin = True
        user.save()
        return user
        
    def init_user(self, username, email, password):
        user = self.model(username=username,
                          email=email)
        user.set_password(password)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='Email Address',
                              max_length=255,
                              unique=True)
    username = models.CharField(max_length=20, 
                                validators=[ASCIIUsernameValidator],
                                unique=True)
    icon_hash = models.CharField(max_length=150)
    about_me = models.CharField(max_length=150, blank=True)
    github_link = models.URLField(verbose_name='github', blank=True)
    linkedin_link = models.URLField(verbose_name='linkedin', blank=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = CustomUserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'password']

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.username

    def save(self, *args, **kwargs):
        identifier = self.email if self.email else self.username
        self.icon_hash = hashlib.md5(identifier.encode()).hexdigest()
        super(User, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('user:profile', kwargs={'username': self.username})

    def get_gravatar_url(self, size=150): 
        return 'https://www.gravatar.com/avatar/{}?s={}&d=identicon'.format(self.icon_hash,
                                                                            size)

    def notify(self, message):
        Notification.objects.create(user=self, text=message)

    def clear_notifications(self):
        return self.notifications.all().delete()

    def get_top_posts(self, count=3):
        return FeedItem.objects.filter(feeds__user=self).all()[:count]

    @property
    def gravatar_url(self):
        return self.get_gravatar_url()

    @property
    def profile_url(self):
        return reverse('user:profile', kwargs={'username': self.username})

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_superuser(self):
        return self.is_admin

    class Meta:
        default_related_name = 'users'

    def __repr__(self):
        return '<User %r>' % self.username


class Notification(models.Model):
    created = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notifications')
    text = models.CharField(max_length=255)
    link = models.URLField(blank=True, null=True)

    class Meta:
        ordering = ('-created',)


class RegistrationRequest(models.Model):
    created = models.DateTimeField(auto_now=True)
    about = models.TextField(verbose_name='About you',
                             max_length=5000,
                             help_text='Please describe a little bit about yourself and/or what you will write about. This is just to prevent spam, I am sure we will be happy to have you!')
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created',)