from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'notifications$', views.NotificationsView.as_view(), name='notifications'),
    url(r'notifications/delete/(?P<pk>[0-9]+)$',
        views.DeleteNotificationView.as_view(), name='delete-notification'),
    url(r'(?P<username>[a-zA-Z0-9]{1,50})/profile$', views.ProfileView.as_view(), name='profile'),
    url(r'(?P<username>[a-zA-Z0-9]{1,50})/profile/edit', views.ProfileEditView.as_view(), name='profile-edit')
]