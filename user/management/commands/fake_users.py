import faker
from django.core.management import BaseCommand
from user.models import User


fake = faker.Faker()

class Command(BaseCommand):
    help = "Create fake users as test data"

    def add_arguments(self, parser):
        parser.add_argument('-c',
                            dest='count',
                            default=5,
                            type=int,
                            help='number of users to create')
        parser.add_argument('-p',
                            dest='password',
                            default='password',
                            help='password for fake users')

    def handle(self, *args, **options):
        for _ in range(options['count']):
            u = User.objects.create_user(fake.user_name(),
                                         fake.safe_email(),
                                         options['password'])
            print(u.__repr__())

        