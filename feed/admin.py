from django.contrib import admin
from .models import Feed


@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'is_private')
    fields = ('user', 'name', 'is_private')