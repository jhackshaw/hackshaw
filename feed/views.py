from django.views.generic import ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponseForbidden
from .models import Feed
from blog.models import FeedItem
from core.models import Tag


User = get_user_model()


class CreateFeedView(CreateView):
    template_name = 'feed/feed_list.html'
    model = Feed
    fields = ['name',]

    def get_context_data(self):
        context = super(CreateFeedView, self).get_context_data()
        page = self.request.GET.get('page', 1)
        username = self.kwargs.get('username')
        feeds = Feed.objects.filter(user__username=username).all()
        paginator = Paginator(feeds, 4)
        
        try:
            context['feeds'] = paginator.page(page)
        except PageNotAnInteger:
            context['feeds'] = paginator.page(1)
        except EmptyPage:
            context['feeds'] = paginator.page(paginator.num_pages)

        context['paginator'] = paginator
        context['popular_feeds'] = Feed.get_popular(5)
        context['popular_tags'] = Tag.get_popular()

        if self.request.user.username != username:
            context['form'] = None
        return context

    def form_valid(self, form):
        if self.request.user.username == self.kwargs['username']:
            form.instance.user = self.request.user
            return super(CreateFeedView, self).form_valid(form)
        return HttpResponseForbidden()


class FeedBase(ListView):
    template_name = 'feed/main_feed.html'
    model = FeedItem
    paginate_by = 5
    paginate_orphans = 2

    def get_context_data(self, *args, **kwargs):
        context = super(FeedBase, self).get_context_data(*args, **kwargs)
        context['popular_feeds'] = Feed.get_popular(5)
        context['popular_tags'] = Tag.get_popular()
        return context


class OverviewFeed(FeedBase):
    def get_queryset(self):
        tag_filter = self.request.GET.get('tag')
        if tag_filter:
            tag = Tag.objects.filter(name=tag_filter).first()
            qs = tag.feeditem_set.filter(published=True).all()
        else:
            qs = FeedItem.objects.filter(published=True).all()
        return qs

    def get_context_data(self):
        context = super(OverviewFeed, self).get_context_data()
        context['tag_filter'] = self.request.GET.get('tag')
        return context


class DraftsFeed(LoginRequiredMixin, OverviewFeed):
    def get_queryset(self):
        user = self.request.user
        tag_filter = self.request.GET.get('tag')
        if tag_filter:
            tag = Tag.objects.filter(name=tag_filter).first()
            qs = tag.feeditem_set.filter(published=False).all()
        else:
            qs = FeedItem.objects.filter(published=False).all()
        return qs.filter(feeds__user=user).all()


class UserFeed(OverviewFeed):
    def get_queryset(self):
        qs = super(UserFeed, self).get_queryset()
        username = self.kwargs.get('username')
        return qs.filter(feeds__user__username=username).all()

    def get_context_data(self, *args, **kwargs):
        context = super(UserFeed, self).get_context_data(*args, **kwargs)
        if self.object_list.count() > 0:
            context['feed_user'] = self.object_list[0].feed.user
        else:
            context['feed_user'] = get_object_or_404(User, username=self.kwargs.get('username'))
        return context


class UserCreatedFeed(UserFeed):
    def get_queryset(self):
        qs = super(UserCreatedFeed, self).get_queryset()
        feedslug = self.kwargs.get('feedslug')
        return qs.filter(feeds__slug=feedslug).all()

    def get_context_data(self, *args, **kwargs):
        context = super(UserCreatedFeed, self).get_context_data(*args, **kwargs)
        if self.object_list.count() > 0:
            context['feed_object'] = self.object_list[0].feed
        else:
            context['feed_object'] = get_object_or_404(Feed, slug=self.kwargs.get('feedslug'))
        return context