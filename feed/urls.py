from django.conf.urls import url, include
from . import views

urlpatterns = [
    # logged in users drafts
    url(r'drafts$', views.DraftsFeed.as_view(), name='drafts'),
    
    # list of user's feeds, also form to create feed if logged in user = username
    # f/user/feeds
    url(r'(?P<username>[a-zA-Z0-9_-]{1,20})/feeds$', 
        views.CreateFeedView.as_view(), 
        name='user-feed-list'),

    # a feed belonging to a specific user
    # f/user/myfeed
    url(r'(?P<username>[a-zA-Z0-9_-]{1,20})/(?P<feedslug>[a-zA-Z0-9_-]{1,100})$', 
        views.UserCreatedFeed.as_view(), 
        name='user-feed'),


    # view a user's feeds
    # f/user
    url(r'(?P<username>[a-zA-Z0-9_-]{1,20})$',
        views.UserFeed.as_view(),
        name='user'),

    # main feed
    url(r'^$', views.OverviewFeed.as_view(), name='overview')
]