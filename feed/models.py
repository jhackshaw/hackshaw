from django.db import models
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.urls import reverse
from blog.models import FeedItem
from gm2m import GM2MField


User = get_user_model()


class Feed(models.Model):
    user = models.ForeignKey(get_user_model(),
                             on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)
    tags = GM2MField('core.Tag', related_name='feeds')
    is_private = models.BooleanField(default=False)
    entries = GenericRelation(FeedItem, related_query_name='feeds')

    class Meta:
        default_related_name = 'feeds'
        unique_together = ('user', 'slug')

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Feed, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def __repr__(self):
        return '<Feed %r>' % self.name

    def get_absolute_url(self):
        return reverse('feed:user-feed', kwargs={'username': self.user.username,
                                                 'feedslug': self.slug})

    @classmethod
    def get_popular(self, count=5):
        return Feed.objects\
                   .annotate(num_posts=models.Count('entries'))\
                   .order_by('-num_posts')\
                   .all()[:count]

    def __repr__(self):
        return '<Feed %r>' % self.name
