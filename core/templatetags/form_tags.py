from django import template
from django.forms import Textarea, Select

register = template.Library()

@register.simple_tag
def form_field_wrapper_classes(field):
    return '' if isinstance(field, Select) else 'input-field'


@register.simple_tag
def form_field(field):
    css = 'validate'
    if field.form.is_bound:
        css += ' invalid' if field.errors else ' valid'
    if isinstance(field.field.widget, Textarea):
        css += ' materialize-textarea'
    return field.as_widget(attrs={'class': field.css_classes(css)})


@register.simple_tag
def form_field_label(field, **kwargs):
    return field.label_tag(attrs={'data-error': ', '.join(field.errors)})


@register.simple_tag
def form_non_field_errors(form):
    return form.non_field_errors()


@register.simple_tag
def form_field_help_text(field):
    pass