from django import template
from django.forms import Textarea, Select

register = template.Library()

@register.simple_tag(takes_context=True)
def replace_url_params(context, **kwargs):
    params = context['request'].GET.copy()
    for k, v in kwargs.items():
        params[k] = v
    return params.urlencode()