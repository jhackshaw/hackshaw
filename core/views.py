from django.shortcuts import render
from django.shortcuts import redirect, reverse
from django.views.generic import View


class RedirectView(View):
    def get(self, request, url):
        return redirect(reverse(url))