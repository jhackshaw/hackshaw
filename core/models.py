from django.db import models
from django.template.defaultfilters import slugify
from user.models import User



class Tag(models.Model):
    name = models.CharField(max_length=20, unique=True)
    slug = models.SlugField(max_length=30, unique=True)


    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(Tag, self).save(*args, **kwargs)

    @staticmethod
    def get_popular(count=15):
        return Tag.objects\
                   .annotate(num_entries=models.Count('feeditem'))\
                   .order_by('-num_entries')\
                   .all()[:count]

    def __repr__(self):
        return '<Tag %r>' % self.name


class Stars(models.Model):
    user = models.ForeignKey(User, related_name='user', on_delete=models.CASCADE)