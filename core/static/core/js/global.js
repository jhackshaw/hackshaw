$(document).ready(function() {
    $('select').material_select();
    $('.button-collapse').sideNav();
    $('textarea').trigger('autoresize');
});

deleteNotification = function(notification_id) {
    var url = '/u/notifications/delete/' + notification_id;
    $.ajax({
        url: url,
        success: function(data) {
            if (data.status === 'deleted') {
                $('#notification_' + notification_id).remove();
            }
        }
    })
}