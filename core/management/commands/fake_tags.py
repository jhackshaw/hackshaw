import random
import faker
from django.core.management import BaseCommand
from core.models import Tag


fake = faker.Faker()


class Command(BaseCommand):
    help = "Create fake tags as test data"

    def add_arguments(self, parser):
        parser.add_argument('-c', 
                            dest='count', 
                            default=20, 
                            type=int, 
                            help='number of fake tags to create')

    def handle(self, *args, **options):
        for _ in range(options['count']):
            to_name = fake.word()
            while Tag.objects.filter(name=to_name).first() is not None:
                print('skipping generation of existing tag')
                to_name = fake.word()
            t = Tag.objects.create(name=to_name)
            print(t.__repr__())